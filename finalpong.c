/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    //MUSICA
    InitAudioDevice();
    
    Music MusicaGameplay = LoadMusicStream("resources/jump.ogg");
    Music Win = LoadMusicStream("resources/starwars.ogg");
    Music Lose = LoadMusicStream("resources/youlose.ogg");
    
    
    //LOGO
    Texture2D logo = LoadTexture("resources/logo.png");
    float alpha = 0.0f;
    bool fadeIn = true;
    
    //TITLE
    Font pixelFont = LoadFont("resources/fuente.ttf");
    char titleText[16] = "FINAL PONG!";
    char startText[16] = "press enter";
    int titleSize = 80;
    Vector2 titlePos = {screenWidth/2 - MeasureTextEx(pixelFont, titleText, titleSize, 0).x/2, -100};
    bool blink = false;
    
    //GAMEPLAY
    Color ballColor = {255, 150, 0, 255};
    Color playerColor = {1, 87, 183, 255};
    Color playerFillColor = {83, 144, 212, 255};
    Color enemyColor = {164, 1, 183, 255}; 
    Color enemyFillColor = {210, 93, 224 ,255};
    
    float topMargin = 50;
    float bgMargin = 10;
    float fieldMargin = 10;
    float counterMargin = 35;
    float lifeMargin = bgMargin;
    
    Rectangle bgRec = {0, topMargin, screenWidth, screenHeight - topMargin};
    Rectangle fieldRec = {bgRec.x + bgMargin, bgRec.y + bgMargin, bgRec.width - bgMargin*2, bgRec.height - bgMargin*2};
    
    Rectangle playerBgRec = {0, 0, screenWidth/2 - counterMargin, topMargin};
    Rectangle playerFillRec = {playerBgRec.x + lifeMargin, playerBgRec.y + lifeMargin, playerBgRec.width - lifeMargin*2, playerBgRec.height - lifeMargin+2};
    Rectangle playerLifeRec = playerFillRec;
    
    Rectangle enemyBgRec = {screenWidth/2 + counterMargin, 0, screenWidth/2 - counterMargin, topMargin};
    Rectangle enemyFillRec = {enemyBgRec.x + lifeMargin, enemyBgRec.y + lifeMargin, enemyBgRec.width - lifeMargin*2, enemyBgRec.height - lifeMargin+2};
    Rectangle enemyLifeRec = enemyFillRec;
    
    Rectangle player = {fieldRec.x + fieldMargin, fieldRec.y + fieldRec.height/2 - 50, 20, 100};
    int playerSpeedY = 4;
    
    Rectangle enemy = {fieldRec.x + fieldRec.width - fieldMargin - 20, fieldRec.y + fieldRec.height/2 - 50, 20, 100};
    int enemySpeedY = 4;
    float AImargin = enemy.height/4;
    
    Vector2 ballPosition = {fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
    Vector2 ballSpeed = { GetRandomValue(5, 7), GetRandomValue(6, 7)};
    if(GetRandomValue(0, 1))ballSpeed.x *= -1;
    if(GetRandomValue(0, 1))ballSpeed.y *= -1;
    
    int ballRadius = 20;
    
    int playerLife = 5;
    int enemyLife = 5;
    
    float damage = playerLifeRec.width/playerLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Lose, 1 - Win, -1 - Not defined
    
    bool pause = false;
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                UpdateMusicStream(MusicaGameplay);
                
                PlayMusicStream(MusicaGameplay);
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if  (fadeIn){
                    alpha += 1.0f/90;
                    
                    if (alpha >= 1.0f){
                        alpha = 1.0f;
                        
                        framesCounter++;
                        if (framesCounter % 120 == 0){
                            framesCounter = 0;
                            fadeIn = false;
                        }
                    }
                }
                else{
                    alpha -= 1.0f/90;
                    
                    if (alpha <= 0.0f){
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                
                if (IsKeyPressed(KEY_ENTER)){
                    framesCounter = 0;
                    screen = TITLE;
                }
                
            } break;
            case TITLE: 
            {
                UpdateMusicStream(MusicaGameplay);
                
                PlayMusicStream(MusicaGameplay);
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (titlePos.y < 100){
                    titlePos.y += 5;
                    
                    if (IsKeyPressed(KEY_ENTER)){
                        titlePos.y= 100;
                    }
                }
                else {
                    titlePos.y = 100;
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                    framesCounter++;
                    if(framesCounter % 20 == 0){
                        framesCounter = 0;
                        blink = !blink;
                    }
                    
                    if (IsKeyPressed(KEY_ENTER)){
                        framesCounter = 0;
                        screen = GAMEPLAY;
                    }                    
                }
                
            } break;
            case GAMEPLAY:
            { 
                UpdateMusicStream(MusicaGameplay);
                
                PlayMusicStream(MusicaGameplay);
                // Update GAMEPLAY screen data here!                   
                if (!pause){
                    // TODO: Ball movement logic.........................(0.2p)
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                    // TODO: Player movement logic.......................(0.2p)
                        if (IsKeyDown(KEY_UP)){
                            player.y -= playerSpeedY;
                        }
                        if (IsKeyDown(KEY_DOWN)){
                            player.y += playerSpeedY;
                        }
                        //Limite pala
                        if (player.y < fieldRec.y) player.y = fieldRec.y;
                        if (player.y > fieldRec.y + fieldRec.height - player.height) player.y = fieldRec.y + fieldRec.height - player.height;
                    // TODO: Enemy movement logic (IA)...................(1p)
                        if ((ballPosition.x > fieldRec.x + fieldRec.width/2) && ballSpeed.x > 0){
                            if (ballPosition.y < enemy.y + enemy.height/2 - AImargin) enemy.y -= enemySpeedY;
                            if (ballPosition.y > enemy.y + enemy.height/2 - AImargin) enemy.y += enemySpeedY;
                        }
                        if (enemy.y < fieldRec.y) enemy.y = fieldRec.y;
                        if (enemy.y > fieldRec.y + fieldRec.height - enemy.height) enemy.y = fieldRec.y + fieldRec.height - enemy.height;
                    // TODO: Collision detection (ball-player) logic.....(0.5p)
                        if(CheckCollisionCircleRec(ballPosition, ballRadius, player) && ballSpeed.x < 0){
                            ballSpeed.x *= -1;
                        }
                    // TODO: Collision detection (ball-enemy) logic......(0.5p)
                        if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy) && ballSpeed.x > 0){
                            ballSpeed.x *= -1;
                        }
                    // TODO: Collision detection (ball-limits) logic.....(1p)
                        //limite superior
                        if (ballPosition.y - ballRadius < fieldRec.y && ballSpeed.y < 0){
                            ballSpeed.y *= -1;
                        }
                        //limite inferior
                        if (ballPosition.y + ballRadius > fieldRec.y + fieldRec.height && ballSpeed.y > 0){
                            ballSpeed.y *= -1;
                        }
                        //limite izquierdo
                        if (ballPosition.x - ballRadius < fieldRec.x && ballSpeed.x < 0){
                            ballSpeed.x *= -1;
                            playerLife--;
                            // TODO: player bars decrease logic
                            playerLifeRec.width -= damage;
                        }
                        //limite derecho
                        if (ballPosition.x + ballRadius > fieldRec.x + fieldRec.width && ballSpeed.x > 0){
                            ballSpeed.x *= -1;
                            enemyLife--;
                            // TODO: enemy bars decrease logic
                            enemyLifeRec.width -= damage;
                            enemyLifeRec.x  += damage;
                        }
                    
                    // TODO: Time counter logic..........................(0.2p)
                    framesCounter++;
                    if (framesCounter % 60 == 0){
                        framesCounter = 0;
                        secondsCounter--;
                    }
                    // TODO: Game ending logic...........................(0.2p)
                        if (secondsCounter <= 0){
                            if (playerLife < enemyLife) gameResult = 0;
                            else if (playerLife > enemyLife) gameResult = 1;                       
                            else gameResult = 2;
                        }
                        else if (playerLife <= 0) gameResult = 0;
                        else if (enemyLife <= 0) gameResult = 1;
                        
                        if (gameResult != -1){
                            screen = ENDING;
                            StopMusicStream(MusicaGameplay);
                        }
                }
                
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed (KEY_P)){pause = !pause;}
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed(KEY_ENTER)){
                    //resetear variables
                        //VIDA
                        playerLife = 5;
                        enemyLife = 5;
                        
                        playerLifeRec = playerFillRec;
                        enemyLifeRec = enemyFillRec;
                        
                        //POSICION PALAS
                        player.y = fieldRec.y + fieldRec.height/2 - 40;
                        enemy.y = player.y;
                        
                        //TIEMPO
                        secondsCounter = 99;
                        framesCounter = 0;
                        
                        //BOLA
                        ballPosition = (Vector2){fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
                        ballSpeed = (Vector2){ GetRandomValue(6, 7), GetRandomValue(6, 7)};
                        if(GetRandomValue(0, 1))ballSpeed.x *= -1;
                        if(GetRandomValue(0, 1))ballSpeed.y *= -1;
                        
                        //PAUSE
                        pause = false;
                        
                        gameResult = -1;
                        
                         screen = GAMEPLAY;
                }
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTexture (logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, Fade(WHITE, alpha));
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawTextEx(pixelFont, titleText, titlePos, titleSize, 0, LIME);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink)DrawText (startText, screenWidth/2 - MeasureText(startText, 20)/2, screenHeight - 100, 20, BLACK);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangleRec(bgRec, BLACK);
                    DrawRectangleRec(fieldRec, RAYWHITE);
                    DrawLineEx((Vector2){bgRec.x + bgRec.width/2, bgRec.y},(Vector2){bgRec.x + bgRec.width/2, bgRec.y + bgRec.height}, bgMargin, BLACK);
                    //BOLA
                    DrawCircleV(ballPosition, ballRadius, ballColor);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, playerColor);
                    DrawRectangleRec(enemy, enemyColor);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(playerBgRec, BLACK);
                    DrawRectangleRec(playerFillRec, playerColor);
                    DrawRectangleRec(playerLifeRec, playerFillColor);
                    
                    DrawRectangleRec(enemyBgRec, BLACK);
                    DrawRectangleRec(enemyFillRec, enemyColor);
                    DrawRectangleRec(enemyLifeRec, enemyFillColor);
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText (FormatText("%i", secondsCounter), screenWidth/2 - MeasureText(FormatText("%i", secondsCounter), 40)/2, topMargin - 45, 40, ballColor);
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause){
                        DrawRectangle(0, 0, screenWidth, screenHeight, Fade(WHITE, 0.8f));
                    DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE", 30)/2, screenHeight/2- 15, 30, BLACK);
                    }
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (gameResult == 0) {
                        UpdateMusicStream(Lose);
                        PlayMusicStream(Lose);
                        DrawText ("YOU LOSE", screenWidth/2 - MeasureText("YOU LOSE", 40)/2, 150, 40, RED);
                    }
                    else if (gameResult == 1) {
                        UpdateMusicStream(Win);
                        PlayMusicStream(Win);
                        DrawText ("YOU WIN", screenWidth/2 - MeasureText("YOU WIN", 40)/2, 150, 40, GREEN);
                    }
                    else if (gameResult == 2) DrawText ("DRAW GAME", screenWidth/2 - MeasureText("DRAW GAME", 40)/2, 150, 40, BLUE);
                    
                    DrawText ("[ENTER] Retry", screenWidth/2 - MeasureText("[ENTER] Retry", 20)/2, screenHeight - 100, 20, BLACK);
                    DrawText ("[ESC] Exit", screenWidth/2 - MeasureText("[ESC] Exit", 20)/2, screenHeight - 75, 20, BLACK);
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadFont(pixelFont);
    UnloadTexture (logo);
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}